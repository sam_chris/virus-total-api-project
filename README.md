Virus Total Public API Interaction
==================================

Tested from a clean install of Ubuntu 14.04

Setup
----

**System wide packages needed - Inlcuding Python 2.7**

    sudo apt-get install libfreetype6-dev python-dev python-pip build-essential libevent-dev libpng-dev

**For matplotlib support:**
    
    sudo apt-get install libfreetype6-dev libevent-dev libpng-dev

**Create Virtual Environment

    virtualenv vtwrapper
    source vtwrapper/bin/activate

Once your virtual environment is working, feel free to install the python package requirements...

**Python packages needed** can be installed using pip and should be done in a virtual environment

    pip install -r requirements.txt

If for some reason this doesn't work. Use pip to install the following packages manually. 

Required
----

    requests
    sqlalchemy
    ipython[notebook]
    
Optional for graphing examples
----
    matplotlib
    numpy
    scipy

Optional for scraping script
---
    beautifulsoup4

Getting Started
---

When these dependencies are installed and working, check out the ipython notebooks in 
    
    <project_root>/vt_wrapper/notebooks/

Then run Ipython Notebook

    ipython notebook


