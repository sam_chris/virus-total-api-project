import json
import datetime

from sqlalchemy import Text, Column, Integer, String, DateTime, ForeignKey, Table
from sqlalchemy.orm import relationship, backref
from database import Base, engine

class SampleReport(Base):
    """Report of sample from Virustotal"""
    __tablename__ = 'samples'
    md5 = Column(String(32), primary_key=True, nullable=False)
    sha1 = Column(String(40), nullable=False)
    sha256 = Column(String(64), nullable=False)
    scan_date = Column(DateTime, nullable=False)

    positives = Column(Integer, nullable=False)
    total = Column(Integer, nullable=False)

    scans = Column(Text, nullable=False)

    vt_link = Column(String(255), nullable=False)

    def __init__(self, vt_json):
        """
        Takes the virustotal response Json and constructs the SQLalchemy model
        @param: vt_json We take the raw response from Virustotal
        """
        self.vt_link = vt_json['permalink']
        self.positives = int(vt_json['positives'])
        self.total = int(vt_json['total'])
        self.md5 = vt_json['md5']
        self.sha1 = vt_json['sha1']
        self.sha256 = vt_json['sha256']
        self.scans = json.dumps(vt_json['scans'])
        self.scan_date = datetime.datetime.strptime(vt_json['scan_date'], "%Y-%m-%d %H:%M:%S")

    def to_json(self):
        res_json = {
            'vt_link': self.vt_link,
            'positives': self.positives,
            'total': self.total,
            'md5': self.md5,
            'sha1': self.sha1,
            'sha256': self.sha256,
            'scans': json.loads(self.scans),
            'scan_date': self.scan_date,
        }

        return res_json


association_table = Table('ip_to_host', Base.metadata,
                          Column('ip_id', Integer, ForeignKey('ips.ip')),
                          Column('hostname_id', Integer, ForeignKey('hostnames.hostname')),
                          Column('last_resolved', DateTime, nullable=True)
                         )


class UrlReport(Base):
    """Report of URL from Virustotal"""
    __tablename__ = 'urls'
    url = Column(String, primary_key=True, unique=True, nullable=False)
    scan_date = Column(DateTime, nullable=False)

    positives = Column(Integer, nullable=False)
    total = Column(Integer, nullable=False)

    # We may not get these when collecting metadata from Ip or Hostname reports, make nullable
    scans = Column(Text, nullable=True)
    vt_link = Column(String(255), nullable=True)

    #Relationship to hostnames
    hostname = Column(String, ForeignKey('hostnames.hostname'))

    def __init__(self, vt_json):
        """
        Takes raw data from either full url reports, or from metadata from Hostnames and URLs
        @params vt_json We take the raw response dictionary
        """
        self.url = vt_json['url']
        self.total = vt_json['total']
        self.scan_date = datetime.datetime.strptime(vt_json['scan_date'], "%Y-%m-%d %H:%M:%S")
        self.positives = vt_json['positives']
        self.total = vt_json['total']

        # These may not be provided, particularly when extracting urls from host and ip queries.
        if vt_json.has_key('scans'):
            self.scans = json.dumps(vt_json['scans'])
        if vt_json.has_key('permalink'):
            self.vt_link = vt_json['permalink']


    def to_json(self):
        res_json = {
            'url': self.url,
            'total': self.total,
            'positives': self.positives,
            'scan_date': self.scan_date,
            'positives': {},
            'vt_link': '',
        }
        if hasattr(self, 'scans'):
            res_json['scans'] = json.loads(self.scans)
        if hasattr(self, 'vt_link'):
            res_json['vt_link'] = self.vt_link

        return res_json

class Hostname(Base):
    """
    Hostnames
    """
    __tablename__ = 'hostnames'
    hostname = Column(String(255), primary_key=True, nullable=False)
    ips = relationship('Ip', secondary=association_table)
    urls = relationship('UrlReport', backref="urls")

    def __init__(self, vt_json):
        """
        Take raw response from virustotal API call
        @param vt_json: raw response from virustotal
        """
        self.hostname = vt_json['hostname']

        # If the resolutions are in the response, then populate IP table based on the response
        if vt_json.has_key('resolutions'):
            self.ips = [Ip(ip_json['ip_address'], ip_json) for ip_json in vt_json['resolutions']]

        # If the detected urls are in the response, then populate the URL table
        if vt_json.has_key('detected_urls'):
            self.urls = [UrlReport(url_json) for url_json in vt_json['detected_urls']]

    def to_json(self):
        res_json = {
            'hostname': self.hostname,
            'ips': [ip.ip for ip in self.ips],
            'urls': [url.url for url in self.urls]
        }
        return res_json

class Ip(Base):
    """
    Ips
    """
    __tablename__ = 'ips'
    ip = Column(String(255), primary_key=True, nullable=False)
    hostnames = relationship('Hostname', secondary=association_table)

    def __init__(self, ip, vt_json):
        """
        Takes raw response from virustotal API call
        @param vt_json raw response from virustotal
        """
        if vt_json.has_key('resolutions'):
            self.hostnames = [Hostname(hostname_json) for hostname_json in vt_json['resolutions']]
        if vt_json.has_key('detected_urls'):
            self.urls = [UrlReport(url_json) for url_json in vt_json['detected_urls']]
        self.ip = ip

    def related_urls(self):
        urls = []
        for hostname in self.hostnames:
            urls.extend(hostname.urls)
        return urls

    def to_json(self):
        res_json = {
            'ip': self.ip,
            'hostnames': [host.hostname for host in self.hostnames],
        }
        return res_json
