from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

#SQL configuration for SQLAlchemy. See: http://docs.sqlalchemy.org/en/rel_0_9/core/engines.html#database-urls
#SQLite used for testing

#Need to find the absolute path here to correctly use the database

import os

this_directory = os.path.dirname(__file__)

# Make sure we get to this database.
engine = create_engine('sqlite:////'+this_directory+'/vt_collection.db',
                       echo=False)

session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

Base = declarative_base()
