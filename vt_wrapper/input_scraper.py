# Garbage code used to scrape some pages for some indicators to scan. I'd prefer you not look to hard at this code ;)

import bs4
import requests
import vt_wrapper
import time


def chunker(iterable, chunk_size):
    for i in xrange(0, len(iterable), chunk_size):
        yield iterable[i:i+chunk_size]


class Populator(object):
    def __init__(self, api_key, sleep=0, per=0):
        self.vtWrapper = vt_wrapper.VtWrapper(api_key)
        self.sleep = sleep
        self.per = per

    def populate_ips(self, ip_file):
        """
        Populates database with new line delimited ip file utilizing VtWrapper class
        @params ip_file, path to ip_file as string ex '/home/user/foobar/some_file'
        """
        with open(ip_file, 'r') as f:
            ips = f.read().split('\n')

        query_count = 0
        for ip in ips:
            if self.per != 0 and query_count % self.per == 0:
                time.sleep(self.sleep)
            self.vtWrapper.get_ip_report(ip)
            query_count += 1

        print("Queried VT for {} ips".format(query_count))
        return query_count

    def populate_hosts(self, host_file):
        """
        Populates database with new line delimited hostname file utilizing the VtWrapper calls
        @param host_file, path the host_file as string ex '/home/user/foobar/some_file'
        """
        with open(host_file, 'r') as f:
            hosts = f.read().split('\n')

        query_count = 0
        for host in hosts:
            if self.per != 0 and query_count % self.per == 0:
                time.sleep(self.sleep)
            self.vtWrapper.get_hostname_report(host)
            query_count += 1

        print("Queried VT for {} hosts".format(query_count))
        return query_count

    def populate_hashes(self, hash_file):
        """
        Populates database with new line delimited hash file utilizing the VtWrapper calls
        @param hash_file, path the hash_file as string ex '/home/user/foobar/some_file'
        """
        with open(hash_file, 'r') as f:
            hashes = f.read().split('\n')

        query_count = 0

        if self.per == 0:
            #one at a time?
            for sample_hash in hashes:
                self.vtWrapper.get_hostname_report([sample_hash])
                query_count += 1
        else:
            for hash_chunk in chunker(hashes, self.per):
                self.vtWrapper.get_hash_report(hash_chunk)
                time.sleep(self.sleep)
                query_count += self.per

        print("Queried VT for {} hashes".format(query_count))
        return query_count

    def populate_urls(self, url_file):
        """
        Populates database with new line delimited url file utilizing the VtWrapper calls
        @param url_file, path the url_file as string ex '/home/user/foobar/some_file'
        """
        with open(url_file, 'r') as f:
            urls = f.read().split('\n')

        query_count = 0

        if self.per == 0:
            for url in urls:
                self.vtWrapper.get_url_report([url])
                query_count += 1
        else:
            for url_chunk in chunker(urls, self.per):
                self.vtWrapper.get_url_report(url_chunk)
                query_count += self.per
                time.sleep(self.sleep)

        print("Queried VT for {} urls".format(query_count))
        return query_count


def scrape_page(some_url):
    response = requests.get(some_url)
    soup = bs4.BeautifulSoup(response.content)
    return soup


def scrape_ips():
    url_to_scrape = 'https://zeustracker.abuse.ch/blocklist.php?download=ipblocklist'
    response = requests.get(url_to_scrape)

    # Top five lines are a header, last one is blank
    content = response.content.split('\n')[6:]
    del content[-1]

    print("Got {} ips from {}".format(len(content), url_to_scrape))
    with open('sample_data/ips', 'w') as f:
        f.write('\n'.join(content))


def scrape_urls():
    url_to_scrape = 'https://zeustracker.abuse.ch/blocklist.php?download=compromised'
    response = requests.get(url_to_scrape)

    # Top five lines are a header, last one is blank
    content = response.content.split('\n')[6:]
    del content[-1]

    urls = [''.join(['http://', line]) for line in content]

    print("Got {} urls from {}".format(len(urls), url_to_scrape))
    with open('sample_data/urls', 'w') as f:
        f.write('\n'.join(urls))


def scrape_hostnames():
    url_to_scrape = 'http://virusshare.com/hashes/VirusShare_00127.md5'
    response = requests.get(url_to_scrape)

    # Top six lines are a header, last one is blank
    content = response.content.split('\n')[7:]
    del content[-1]

    print("Got {} hostnames from {}".format(len(content), url_to_scrape))
    with open('sample_data/hostnames', 'w') as f:
        f.write('\n'.join(content))


def scrape_sample_hashes():

    url_to_scrape = 'http://www.malc0de.com/database'
    soup = scrape_page(url_to_scrape)

    trs = soup.table.findAll('tr')

    #Top row is a header
    del trs[0]

    hashes = [tr.findAll('td')[-1].text for tr in trs]

    #Append to file
    print("Got {} hashes from {}".format(len(hashes), url_to_scrape))
    with open('sample_data/hashes', 'w') as f:
        f.write('\n'.join(hashes))
        f.write('\n')


def scrape_all():
    print("scraping hostnames, urls, samples, and ips and dumping them in sample_data")
    print("Scraping urls")
    scrape_ips()

    scrape_urls()
    time.sleep(60)
    scrape_hostnames()
    time.sleep(60)
    scrape_sample_hashes()


def populate_db():
    populator = Populator("c8cd25eabf26dff8cb17df8095c0de3560877e2e7c16d0c61ef717e9c5d829ac", sleep=60, per=4)
    print("populating urls")
    populator.populate_urls('sample_data/urls')
    print("populating hostnames")
    time.sleep(60)
    populator.populate_hosts('sample_data/hostnames')
    print("populating hashes")
    time.sleep(60)
    populator.populate_hashes('sample_data/hashes')
    print("populating ips")
    time.sleep(60)
    populator.populate_ips('sample_data/ips')


if __name__ == '__main__':
    #scrape_all()
    populate_db()