# filename: vt_wrapper.py
# author: Samuel Christian
# bitbucket: sam_chris
#
# Description: Wrapper providing calls to Virustotal, can make calls that automatically push to db and
# return report objects, or can return json responses that do not get pushed to the database
#
# Sample Usage:
#    wrapper = VtWrapper('myapikey')
#    hostReport = wrapper.get_host_report('awesome.com')
#    host_json = wrapper.josn_host_report('awesome.com')
#
#    Bulk queries supported for URLs and Hashes
#    hashReports = wrapper.get_hash_report(['md5-1', 'md5-2'])
#
# EXCEPTIONS:
#    RateLimited - raised if the Virustotal rate was capped - free api key allows 4 a minute. :(
#
# Sample API Key c8cd25eabf26dff8cb17df8095c0de3560877e2e7c16d0c61ef717e9c5d829ac

import functools
import requests
import sys

sys.path.append("../sql_db/")
from models import *
from database import session, engine


############
# Decorators
############

def push_to_db(func_to_decorate):
    @functools.wraps(func_to_decorate)
    def wrapper(*args, **kwargs):
        result = func_to_decorate(*args, **kwargs)
        #Push to db if the result exists
        #If a list was returned, add all the items to the session.
        #Sometimes we hit an error typically with a rate limited, or not found request.
        #The error handling could, and should, be more fruitful
        if result and type(result) is list:
            for item in result:
                try:
                    session.merge(item)
                except Exception, e:
                    print("Problem merging. {}".format(e))

        elif result:
            try:
                session.merge(result)
            except Exception, e:
                print("Problem merging. {}".format(e))

        try:
            session.commit()
        except Exception, e:
            print("Problem committing. {}".format(e))
            print("Rolling back")
            session.rollback()

        return result
    return wrapper

############
# Exceptions
############

class RateLimited(Exception):
    pass


class VtWrapper(object):
    def __init__(self, api_key):
        """
        Interacting with the VirusTotal API
        Provide it with an API key
        """
        self._api_key = api_key
        self._api_root = 'https://www.virustotal.com/vtapi/v2/'
        self._hostname_url = self._api_root+'domain/'
        self._hash_url = self._api_root + 'file/'
        self._url_url = self._api_root + 'url/'
        self._ip_url = self._api_root + 'ip-address/'

    @push_to_db
    def get_hostname_report(self, hostname):
        """
        Queries VirusTotal for the given hostname and returns the report.
        If the hostname has already been stored on our end, it is updated with the new information
        otherwise we simply create an object and push it to the database
        @param: hostname: hostname as a string
        @return: none if nothing is found, or a HostnameReport object
        @raises: RateLimited if rate limit for Virustotal is reached
        """
        params = {'domain': hostname,
                  'apikey': self._api_key}
        response = requests.get(self._hostname_url + 'report', params=params)
        was_rate_limited(response)

        if response.status_code == 200 and response.json()['response_code'] == 1:
            response_json = response.json()
            #Need to tack on the query since VT doesn't respond with it.
            response_json['hostname'] = hostname
            hostReport = Hostname(response_json)

        return hostReport

    @push_to_db
    def get_ip_report(self, ip):
        """
        Queries VirusTotal for the given ip and returns the report.
        If the ip has already been stored on our end, it is updated with the new information
        otherwise we simply create an object and push it to the database
        @param: ip: ip as a string
        @return: None if nothing is found, or an IpReport object
        @raises: RateLimited if rate limit for Virustotal is reached
        """
        params = {'ip': ip,
                  'apikey': self._api_key}
        response = requests.get(self._ip_url + 'report', params=params)
        was_rate_limited(response)

        if response.status_code == 200 and response.json()['response_code'] == 1:
            response_json = response.json()
            #Need to tack on the query since VT doesn't response with it
            response_json['ip'] = ip
            ipReport = Ip(ip, response_json)

        return ipReport

    @push_to_db
    def get_hash_report(self, hashes):
        """
        Queries VirusTotal for the given hash and returns the report.
        If the hash has already been stored on our end, it is updated with the new information
        otherwise we simply create an object and push it to the database
        @param: hashes: hashes as a list of strings, sha1 sha256 or md5
        @return: None if nothing is found, or a list of HashReport objects
        @raises: RateLimited if rate limit for Virustotal is reached
        """
        params = {'resource': ', '.join(hashes),
                  'apikey': self._api_key}
        response = requests.get(self._hash_url + 'report', params=params)
        was_rate_limited(response)

        if response.status_code == 200:
            json_response = response.json()
            if type(json_response) is not list:
                json_response = [json_response]
            items = [SampleReport(json_data) if json_data['response_code'] == 1 else '' for json_data in json_response]

        else:
            print("Warning! Report query for {} returned a {} code".format(', '.join(hashes), response.status_code))
            items = []

        return items

    @push_to_db
    def get_url_report(self, urls):
        """
        Queries VirusTotal for the given url and returns the report.
        If the url has already been stored on our end, it is updated with the new information
        otherwise we simply create an object and push it to the database
        @param: urls: a list of strings
        @return: None if nothing is found, or a list of UrlReport objects
        @raises: RateLimited if rate limit for Virustotal is reached
        """
        params = {'resource': '\n'.join(urls),
                  'apikey': self._api_key}

        response = requests.get(self._url_url + 'report', params=params)
        was_rate_limited(response)

        if response.status_code == 200:
            json_response = response.json()
            if type(json_response) is not list:
                json_response = [json_response]
            items = [UrlReport(json_data) if json_data['response_code'] == 1 else '' for json_data in json_response]
        else:
            print("Warning: Report query for {} returned a {} code".format(', '.join(urls), response.status_code))
            items = []

        return items

    def scan_url(self, urls):
        """
        Posts a URL to scan to virustotal
        @param: urls as a list of strings
        @return: Boolean True or False
        @raises: RateLimited if rate limit for Virustotal is reached
        """
        result = False
        params = {'url': '\n'.join(urls),
                  'apikey': self._api_key}
        response = requests.post(self._url_url + 'scan', params=params)
        was_rate_limited(response)

        if response.status_code == 200:
            result = True
        else:
            result = False

        return result

    def add_comment(self, resource, comment):
        """
        HTTP Puts a comment to a resource, either a sample hash, or the url itself.
        @param: resource: String For sample: MD5, sha1, sha25
                                     ULR: Url
        @return: Boolean True or False
        @raises: RateLimited if rate limit for Virustotal is reached
        """
        result = False
        params = {'resource': resource,
                  'comment': comment,
                   'apikey': self._api_key}

        response = requests.put(self._api_root + 'comments/put', params=params)
        was_rate_limited(response)

        if response.status_code == 200:
            result = True
        else:
            result = False

        return result

    def submit_sample(self, sample):
        """
        Uses HTTP multipart post to post a sample to virustotal
        @param: sample, file handle of sample (--- open('somefile', 'r') ---)
        @return: True or False if the sample was submitted.
        @raises: RateLimited if rate limit for Virustotal is reached
        """
        result = False
        params = {'apikey': self._api_key}
        files = {'file': sample}
        response = requests.post(self._hash_url + 'scan', params=params, files=files)
        was_rate_limited(response)

        if response.status_code == 200:
            result = True
        else:
            result = False

        return result

    ####################
    # Json Report Calls
    ####################

    def json_hash_report(self, hashes):
        """
        Simple json wrapper for Viustotal Query for hashes
        @param hashes: List of sha1, md5, or sha256 hashes as strings
        @return: list of dictionary reports for hashes
        @raises: RateLimited if rate limit for Virustotal is reached
        """
        params = {'resource': ', '.join(hashes),
                  'apikey': self._api_key}
        response = requests.get(self._hash_url + 'report', params=params)
        was_rate_limited(response)

        if response.status_code == 200:
            json_response = response.json()
            if type(json_response) is not list:
                #Make response always a list
                json_response = [json_response]

            return json_response

        return None

    def json_url_report(self, urls):
        """
        Simple json wrapper for Virustotal Query for urls
        @param urls: List of URLs as strings
        @return: list of dictionary reports for urls
        @raises: RateLimited if rate limit for Virustotal is reached
        """
        params = {'resource': '\n'.join(urls),
                  'apikey': self._api_key}

        response = requests.get(self._url_url + 'report', params=params)
        was_rate_limited(response)

        if response.status_code == 200:
            json_response = response.json()
            if type(json_response) is not list:
                # Make response always a list
                json_response = [json_response]

            return json_response

        return None

    def json_ip_report(self, ip):
        """
        Simple json wrapper for Virustotal ip query
        @param ip: Quadded IP as a string (IPv4)
        @return: dictionary of report for ip
        @raises: RateLimited if rate limit for Virustotal is reached
        """
        params = {'ip': ip,
                  'apikey': self._api_key}
        response = requests.get(self._ip_url + 'report', params=params)
        was_rate_limited(response)

        if response.status_code == 200 and response.json()['response_code'] == 1:
            response_json = response.json()
            return response_json

        return None

    def json_hostname_report(self, hostname):
        """
        Simple json wrapper for Virustotal hostname query
        @param hostname: Hostname as a string
        @return: dictionary of report for hostname
        @raises: RateLimited if rate limit for Virustotal is reached
        """
        params = {'domain': hostname,
                  'apikey': self._api_key}
        response = requests.get(self._hostname_url + 'report', params=params)
        was_rate_limited(response)

        if response.status_code == 200 and response.json()['response_code'] == 1:
            response_json = response.json()
            return response_json

        else:
            return None


###########
# Functions
###########

def was_rate_limited(http_response):
    """
    @param: http_response: response from requests module
    @raises: RateLimited if rate limit was exceeded
    """
    if http_response.status_code == 204:
        raise RateLimited
